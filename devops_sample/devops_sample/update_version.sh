#!/bin/bash

# get the date and SHA of the last commit
commitDate=$(git log -1 --format=%cd --date=format:'%y%m%d')
commitSHA=$(git rev-parse --short HEAD)

# construct the new version
newVersion="${commitDate}-${commitSHA}"

# update the project version
mvn versions:set -DnewVersion=$newVersion

